﻿using Facebook.Login.Models;
using Facebook.Login.Resources;
using Facebook.Login.Shared;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Facebook.Login.Services
{
    public class FacebookServices : IFacebookServices
    {
        public static HttpClient client;

        public FacebookServices()
        {
            client = new HttpClient();
        }

        public async Task<FacebookProfileResponse> GetFacebookProfileAsync(string accessToken)
        {
            FacebookProfileResponse responseData = new FacebookProfileResponse();

            try
            {
                Uri uri = new Uri(Constants.ProfileRequestURL + accessToken);

                HttpResponseMessage response = await client.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();
                    responseData.UserData = JsonConvert.DeserializeObject<FacebookProfileModel>(content);
                    responseData.Code = (int)ResponseCodes.Ok;
                }
                else
                {
                    responseData.Message += $" (Error Code {(int)response.StatusCode}: {response.ReasonPhrase})";
                }
            }
            catch (Exception ex)
            {
                responseData.Message = ex.Message;
            }

            return responseData;
        }
    }
}
