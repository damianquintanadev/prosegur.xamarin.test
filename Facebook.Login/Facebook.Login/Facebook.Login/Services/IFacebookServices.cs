﻿using Facebook.Login.Models;
using System.Threading.Tasks;

namespace Facebook.Login.Services
{
    public interface IFacebookServices
    {
        Task<FacebookProfileResponse> GetFacebookProfileAsync(string accessToken);
    }
}
