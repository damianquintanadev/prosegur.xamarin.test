﻿namespace Facebook.Login.Shared
{
    public static class Constants
    {
        public const string FacebookClientId = "963690317715657";
        public const string LoginRequestURL = "https://www.facebook.com/dialog/oauth?client_id={0}&display=popup&response_type=token&redirect_uri={1}";
        public const string ProfileRequestURL = "https://graph.facebook.com/v2.7/me/?fields=name,picture,first_name,last_name&access_token=";
        public const string SuccessRedirectURL = "https://blank.page/";
    }
}
