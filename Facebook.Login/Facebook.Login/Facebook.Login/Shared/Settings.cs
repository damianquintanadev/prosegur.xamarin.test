﻿
using Plugin.Settings;
using Plugin.Settings.Abstractions;

namespace Facebook.Login.Shared
{
    public static class Settings
    {
        const string UserDataKey = "UserDataKey";
        private static readonly string UserDataDefault = string.Empty;

        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }

        public static string UserData
        {
            get { return AppSettings.GetValueOrDefault(UserDataKey, UserDataDefault); }
            set { AppSettings.AddOrUpdateValue(UserDataKey, value); }
        }
    }
}
