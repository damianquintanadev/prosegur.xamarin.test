﻿using System;

namespace Facebook.Login.Models
{
    public class FlyoutPageItem
    {
        public string Title { get; set; }
        public Type TargetType { get; set; }
        public bool Selected { get; set; }
    }
}
