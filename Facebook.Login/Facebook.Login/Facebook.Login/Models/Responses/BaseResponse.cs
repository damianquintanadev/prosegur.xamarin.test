﻿using Facebook.Login.Resources;

namespace Facebook.Login.Models
{
    public class BaseResponse
    {
        public string Message { get; set; }
        public int Code { get; set; }

        public BaseResponse()
        {
            Code = (int)ResponseCodes.Error;
            Message = TextResources.RequestFacebookError;
        }
    }

    public enum ResponseCodes
    {
        Ok,
        Error
    }
}
