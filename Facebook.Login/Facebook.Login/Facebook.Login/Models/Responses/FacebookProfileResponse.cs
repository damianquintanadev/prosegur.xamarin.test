﻿namespace Facebook.Login.Models
{
    public class FacebookProfileResponse : BaseResponse
    {
        public FacebookProfileModel UserData { get; set; }
    }
}
