﻿using Facebook.Login.Shared;
using Facebook.Login.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Facebook.Login.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginFacebook : ContentPage
    {
        public LoginFacebook()
        {
            InitializeComponent();
            BindingContext = new LoginFacebookViewModel(this);

            var apiRequest = string.Format(Constants.LoginRequestURL, Constants.FacebookClientId, Constants.SuccessRedirectURL);

            webView = new WebView
            {
                Source = apiRequest,
                HeightRequest = 1
            };

            webView.Navigated += WebViewOnNavigated;

            Content = webView;
        }

        private async void WebViewOnNavigated(object sender, WebNavigatedEventArgs e)
        {
            var vm = BindingContext as LoginFacebookViewModel;

            var accessToken = GetAccessTokenFromUrl(e.Url);
            if (!string.IsNullOrEmpty(accessToken))
            {
                webView.IsVisible = false;
                vm.IsBusy = true;
                await vm.SetFacebookUserProfileAsync(accessToken);
            }
        }

        private string GetAccessTokenFromUrl(string url)
        {
            if (url.Contains("access_token") && url.Contains("&data_access_expiration_time="))
            {
                var at = url.Replace($"{Constants.SuccessRedirectURL}?#access_token=", "");

                return at.Remove(at.IndexOf("&data_access_expiration_time="));
            }

            return string.Empty;
        }
    }
}