﻿
using Facebook.Login.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Facebook.Login.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        public Login()
        {
            InitializeComponent();

            BindingContext = new LoginViewModel(this);
        }
    }
}