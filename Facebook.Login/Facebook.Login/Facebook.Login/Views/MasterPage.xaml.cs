﻿using Facebook.Login.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Facebook.Login.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterPage : ContentPage
    {
        public MasterPage()
        {
            InitializeComponent();
            BindingContext = new MasterPageViewModel(this);
        }
    }
}