﻿using Facebook.Login.Models;
using Facebook.Login.Native;
using Facebook.Login.Resources;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Facebook.Login.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Main : FlyoutPage
    {
        private static Page displayPage;

        public Main()
        {
            InitializeComponent();
            displayPage = (Page)Activator.CreateInstance(typeof(Profile));
            Detail = new NavigationPage(displayPage)
            {
                BarTextColor = Color.White
            };

            flyoutPage.listView.ItemSelected += OnItemSelected;
        }

        private void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem is FlyoutPageItem item)
            {
                if (item.TargetType != null)
                {
                    Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                    flyoutPage.listView.SelectedItem = null;
                    IsPresented = false;
                }
                else
                {
                    App.CloseSession();
                }
            }
        }

        protected override bool OnBackButtonPressed()
        {
            if (Detail.Navigation.NavigationStack.Count > 1)
            {
                return base.OnBackButtonPressed();
            }

            if (Detail.Navigation.NavigationStack[0].GetType() == typeof(Profile))
            {
                Task<bool> action = DisplayAlert(TextResources.AppTitle, TextResources.QuestionExit, TextResources.Yes, TextResources.Cancel);
                action.ContinueWith(task =>
                {
                    if (task.Result)
                    {
                        if (Device.RuntimePlatform == Device.Android)
                        {
                            DependencyService.Get<IAndroidMethods>().CloseApp();
                        }
                        return base.OnBackButtonPressed();
                    }
                    else
                    {
                        return true;
                    }
                });
            }

            return true;
        }
    }
}