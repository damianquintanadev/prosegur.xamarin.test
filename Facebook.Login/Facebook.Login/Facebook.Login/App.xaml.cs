﻿using Facebook.Login.Models;
using Facebook.Login.Services;
using Facebook.Login.Shared;
using Nancy.TinyIoc;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Facebook.Login
{
    public partial class App : Application
    {
        public static TinyIoCContainer Container;

        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new Views.Login());

            RegisterServices();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }

        public static void SaveUserData(FacebookProfileModel data)
        {
            Settings.UserData = JsonConvert.SerializeObject(data);
        }

        public static FacebookProfileModel GetUserData()
        {
            if (!string.IsNullOrEmpty(Settings.UserData))
            {
                return JsonConvert.DeserializeObject<FacebookProfileModel>(Settings.UserData);
            }
            return null;
        }

        public static void CloseSession()
        {
            Current.MainPage = new NavigationPage(new Views.Login());
        }

        private void RegisterServices()
        {
            Container = new TinyIoCContainer();
            Container.Register<IFacebookServices, FacebookServices>();
        }
    }
}
