﻿using Facebook.Login.Models;
using Facebook.Login.Resources;
using Facebook.Login.Services;
using Facebook.Login.Views;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Facebook.Login.ViewModels
{
    public class LoginFacebookViewModel : BaseViewModel
    {
        private IFacebookServices FacebookServices = App.Container.Resolve<IFacebookServices>();

        #region Constructors
        public LoginFacebookViewModel(Page page) : base(page)
        {
        }

        #endregion

        public async Task SetFacebookUserProfileAsync(string accessToken)
        {
            await BusyAsync(async () =>
            {
                FacebookProfileResponse response = await FacebookServices.GetFacebookProfileAsync(accessToken);

                if (response.Code == (int)ResponseCodes.Ok)
                {
                    App.SaveUserData(response.UserData);
                    Application.Current.MainPage = new Main();
                }
                else
                {
                    await ShowAlert(TextResources.AppTitle, response.Message, TextResources.Ok);
                    App.CloseSession();
                }
            });
        }
    }
}
