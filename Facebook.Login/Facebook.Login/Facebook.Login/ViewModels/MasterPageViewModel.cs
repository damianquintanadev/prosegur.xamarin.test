﻿using Facebook.Login.Models;
using Facebook.Login.Resources;
using Facebook.Login.Views;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace Facebook.Login.ViewModels
{
    public class MasterPageViewModel : BaseViewModel
    {
        public ObservableCollection<FlyoutPageItem> Menu { get; set; } = new ObservableCollection<FlyoutPageItem>();

        public MasterPageViewModel(Page page) : base(page)
        {
            Menu.Add(new FlyoutPageItem
            {
                Title = TextResources.Profile,
                TargetType = typeof(Profile)
            });
            Menu.Add(new FlyoutPageItem
            {
                Title = TextResources.CloseSession
            });
        }
    }
}
