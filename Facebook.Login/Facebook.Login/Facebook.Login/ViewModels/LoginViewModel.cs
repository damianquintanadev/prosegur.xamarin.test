﻿using Facebook.Login.Views;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Facebook.Login.ViewModels
{
    public class LoginViewModel : BaseViewModel
    {
        public ICommand LoginCommand { get; }

        #region Constructors
        public LoginViewModel(Page page) : base(page)
        {
            LoginCommand = new Command(async () => await Login());
        }

        #endregion

        private async Task Login()
        {
            if (IsBusy)
                return;

            await BusyAsync(async () =>
            {
                await Page.Navigation.PushAsync(new LoginFacebook());
            });
        }
    }
}
