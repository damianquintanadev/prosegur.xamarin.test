﻿using Facebook.Login.Resources;
using Facebook.Login.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Facebook.Login.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        #region Members
        protected readonly Page Page;
        bool isBusy;
        private string busyMessage = TextResources.Loading;
        #endregion

        #region Properties
        public bool IsBusy
        {
            get { return isBusy; }
            set { SetProperty(ref isBusy, value); }
        }

        public string BusyMessage
        {
            get { return busyMessage; }
            set { SetField(ref busyMessage, value); }
        }

        #endregion

        #region Constructor

        public BaseViewModel() { }

        public BaseViewModel(Page page)
        {
            this.Page = page;
        }

        #endregion

        protected bool SetProperty<T>(ref T backingStore, T value,
          [CallerMemberName] string propertyName = "",
          Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return false;

            field = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        public async Task BusyAsync(Func<Task> action, string message = "Aguarde un instante...")
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                this.BusyMessage = message;
                this.IsBusy = true;
            });
            try
            {
                await action();
            }
            catch (Exception ex)
            {
                Device.BeginInvokeOnMainThread(async () =>
                {
                    this.IsBusy = false;
                    await ShowAlert(TextResources.AppTitle, TextResources.UnexpectedError, TextResources.Ok);
                });
            }
            finally
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    this.IsBusy = false;
                });
            }
        }

        public async Task ShowAlert(string title, string message, string cancel)
        {
            await this.Page.DisplayAlert(title, message, cancel);
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
