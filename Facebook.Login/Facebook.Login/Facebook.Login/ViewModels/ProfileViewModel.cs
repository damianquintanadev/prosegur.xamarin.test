﻿using Facebook.Login.Models;
using Facebook.Login.Resources;
using Facebook.Login.Services;
using Facebook.Login.Views;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Facebook.Login.ViewModels
{
    public class ProfileViewModel : BaseViewModel
    {
        #region Members
        private FacebookProfileModel facebookProfile;
        #endregion

        #region Properties
        public FacebookProfileModel FacebookProfile
        {
            get { return facebookProfile; }
            set
            {
                facebookProfile = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Constructors
        public ProfileViewModel(Page page) : base(page)
        {
            FacebookProfile = App.GetUserData();
        }

        #endregion
    }
}
