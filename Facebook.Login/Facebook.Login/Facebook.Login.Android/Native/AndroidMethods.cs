﻿using Android.OS;
using Facebook.Login.Droid.Native;
using Facebook.Login.Native;


[assembly: Xamarin.Forms.Dependency(typeof(AndroidMethods))]
namespace Facebook.Login.Droid.Native
{
    public class AndroidMethods : IAndroidMethods
    {
        public void CloseApp()
        {
            Process.KillProcess(Process.MyPid());
        }
    }
}